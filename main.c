#include <stdio.h>
#include <stdlib.h>
char mod;
int m,n,k,a[1010][1010],i,j,vecin,pasul,b[1010][1010];
float grad_maxim, grad_de_populare;
int main()
{
    printf("Selectati modul jocului: ");
    scanf("%c", &mod);
    if(mod!='p' && mod!='P' && mod!='t' && mod!='T')
        printf("Modul de joc ales nu este corect");
    else
    {
        printf("Numarul de coloane si de linii: ");
        scanf("%d %d", &m, &n);
        printf("Numarul de generatii de simulat: ");
        scanf("%d", &k);
        printf("Introduceti pozitia initiala a jocului: ");
        for(i=1; i<=n; i++)
            for(j=1; j<=m; j++)
            {
                scanf("%d", &a[i][j]);
                if(a[i][j]==1)
                    grad_de_populare++;
            }
        if(mod == 'p' || mod == 'P')   //in modul plan, bordam matricea cu 0
        {
            for(j=0; j<=m+1; j++)
            {
                a[0][j]=0;
                a[n+1][j]=0;
            }
            for(i=0; i<=n+1; i++)
            {
                a[i][0]=0;
                a[i][m+1]=0;
            }
        }
        else if((n==2 && m==1) || (n==2 && m==2) || (n==1 && m==2) || (n==1 && m==1) )
            //cazurile speciale, din modul toroid, in care bordarea trebuie sa fie 0
        {
            for(j=0; j<=m+1; j++)
            {
                a[0][j]=0;
                a[n+1][j]=0;
            }
            for(i=0; i<=n+1; i++)
            {
                a[i][0]=0;
                a[i][m+1]=0;
            }
        }
        else if(m<3)  //cazul in care numarul de coloane este 1 sau 2
        {
            for(i=1; i<=2; i++)
            {
                a[0][i]=a[n][i];
                a[n+1][i]=a[1][i];
            }
            for(i=0; i<=n+1; i++)
            {
                a[i][0]=0;
                a[i][m+1]=0;
            }
        }
        else if(n<3)  //cazul in care numarul de linii este 1 sau 2
        {
            for(j=0; j<=m+1; j++)
            {
                a[0][j]=0;
                a[n+1][j]=0;
            }
            for(i=1; i<=2; i++)
            {
                a[i][0]=a[i][m];
                a[i][m+1]=a[i][1];
            }
        }
        else if(m>=3 && n>=3)
        {
            /*
            In modul toroid bordam matricea la Nord cu ultima linie, la Est cu prima coloana,
            la Sud cu prima linie si la Vest cu ultima coloana.
            De asemenea, colturile vor fi bordate cu opusele lor.
            */
            for(j=1; j<=m; j++)
            {
                a[0][j]=a[n][j];
                a[n+1][j]=a[1][j];
            }
            for(i=0; i<=n+1; i++)
            {
                a[i][0]=a[i][m];
                a[i][m+1]=a[i][1];
            }
            a[0][0]=a[n][m];
            a[0][m+1]=a[n][1];
            a[n+1][0]=a[m][1];
            a[n+1][m+1]=a[1][1];
        }
        grad_de_populare=(float)grad_de_populare/(n*m)*100;
        grad_maxim=grad_de_populare;
        while(pasul<k)
        {
            // intializam mereu gradul de populare a matricei cu 0
            grad_de_populare=0;
            // sa nu uitam ca matricea trebuie mordata (in modul toroid) la fiecare pas
            if(((n==2 && m==1) || (n==2 && m==2) || (n==1 && m==2) || (n==1 && m==1)) && (mod =='T' || mod == 't') )
                //cazurile speciale in care bordarea trebuie sa fie 0
            {
                for(j=0; j<=m+1; j++)
                {
                    a[0][j]=0;
                    a[n+1][j]=0;
                }
                for(i=0; i<=n+1; i++)
                {
                    a[i][0]=0;
                    a[i][m+1]=0;
                }
            }
            else if(m<3 && (mod =='T' || mod == 't'))  //cazul in care numarul de coloane este 1 sau 2
            {
                for(i=1; i<=2; i++)
                {
                    a[0][i]=a[n][i];
                    a[n+1][i]=a[1][i];
                }
                for(i=0; i<=n+1; i++)
                {
                    a[i][0]=0;
                    a[i][m+1]=0;
                }
            }
            else if(n<3 && (mod =='T' || mod == 't'))  //cazul in care numarul de linii este 1 sau 2
            {
                for(j=0; j<=m+1; j++)
                {
                    a[0][j]=0;
                    a[n+1][j]=0;
                }
                for(i=1; i<=2; i++)
                {
                    a[i][0]=a[i][m];
                    a[i][m+1]=a[i][1];
                }
            }
            else if((m>=3 && n>=3) && (mod =='T' || mod == 't'))
            {
                /*
                In modul toroid bordam matricea la Nord cu ultima linie, la Est cu prima coloana,
                la Sud cu prima linie si la Vest cu ultima coloana.
                De asemenea, colturile vor fi bordate cu opusele lor.
                */
                for(j=1; j<=m; j++)
                {
                    a[0][j]=a[n][j];
                    a[n+1][j]=a[1][j];
                }
                for(i=0; i<=n+1; i++)
                {
                    a[i][0]=a[i][m];
                    a[i][m+1]=a[i][1];
                }
                a[0][0]=a[n][m];
                a[0][m+1]=a[n][1];
                a[n+1][0]=a[m][1];
                a[n+1][m+1]=a[1][1];
            }

            for(i=1; i<=n; i++)
                for(j=1; j<=m; j++)
                {
                    //aflarea numarului de vecini ai fiecarei celule
                    vecin=0;
                    if(a[i+1][j-1]!=0)  vecin++;
                    if(a[i][j-1]!=0)    vecin++;
                    if(a[i-1][j-1]!=0)  vecin++;
                    if(a[i-1][j]!=0)    vecin++;
                    if(a[i-1][j+1]!=0)  vecin++;
                    if(a[i][j+1]!=0)    vecin++;
                    if(a[i+1][j+1]!=0)  vecin++;
                    if(a[i+1][j]!=0)    vecin++;
                    //celulele care supravietuiesc vor avea valoarea 2, la fel si cele nou create
                    if(vecin==2 && a[i][j]==1) a[i][j]=2;
                    if(vecin==3 && a[i][j]==1) a[i][j]=2;
                    //salvam in matricea b, noile celule create
                    if(vecin==3 && a[i][j]==0) b[i][j]=2;
                }
            //celulele nou create sunt mutate din matricea b in a
            for(i=1; i<=n; i++)
                for(j=1; j<=m; j++)
                    if(b[i][j]!=0)
                    {
                        a[i][j]=2;
                        b[i][j]=0;
                    }
            // celulele care nu au indeplinit conditiile, mor de singuratate sau de supraaglomerare
            // celulele care au supravietuit, revin la valoarea de 1
            for(i=1; i<=n; i++)
                for(j=1; j<=m; j++)
                {
                    if(a[i][j]!=0)
                        a[i][j]--;
                    if(a[i][j]==1)
                        grad_de_populare++;
                }
            // recalcularea gradului de populare si verificarea daca este cel mai mare pana la pasul respectiv
            grad_de_populare=(float)grad_de_populare/(n*m)*100;
            grad_maxim=(grad_maxim>grad_de_populare)?grad_maxim:grad_de_populare;
            pasul ++;
        }

        printf("\n");
        for(i=1; i<=n; i++)     //afisarea matricei
        {
            for(j=1; j<=m; j++)
                printf("%d ",a[i][j]);
            printf("\n");
        }
        printf("%.3lf %%",grad_maxim);
    }
    return 0;
}
